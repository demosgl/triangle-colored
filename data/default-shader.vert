#version 150

in vec3 vertex;
in vec3 color;

uniform mat4 projection_matrix;

out vec4 var_color;

void main()
{
    gl_Position = projection_matrix * vec4(vertex.xyz, 1.0);
    var_color = vec4(color.xyz, 1.0);
}
