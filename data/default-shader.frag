#version 150

in vec4 var_color;

out vec4 color;

void main()
{
    color = var_color;
}
