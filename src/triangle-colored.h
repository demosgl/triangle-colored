#ifndef TRIANGLE_COLORED_PROGRAM_H
#define TRIANGLE_COLORED_PROGRAM_H

#include <program.h>
#include <GL/glextl.h>
#include <glm/glm.hpp>

class TriangleColoredProgram : public Program
{
public:
    TriangleColoredProgram() : Program("Triangle Colored - demos.gl") { }

    virtual bool Initialize();
    virtual void Render(int width, int height);
    virtual void Destroy();

    virtual void SetupShaders();
    virtual void SetupBuffers();
private:
    GLuint shader;
    GLint projection_matrix;
    GLuint vbo;
    GLuint vao;

    static std::string LoadShaderFile(const std::string& filename);
    void CheckShaderStatus(GLuint shader);
    void CheckProgramStatus(GLuint program);

    static glm::vec3 vertices[];

};

#endif  // TRIANGLE_COLORED_PROGRAM_H
