#include "triangle-colored.h"
#include <GL/glextl.h>
#include <iostream>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

bool TriangleColoredProgram::Initialize()
{
    glClearColor(0.3f, 0.3f, 0.3f, 1.0f);

    this->SetupShaders();

    this->SetupBuffers();

    return true;
}

void TriangleColoredProgram::Render(int width, int height)
{
    glm::mat4 projection = glm::perspective(glm::radians(60.0f), float(width) / float(height <= 0 ? 1 : height), 0.1f, 200.0f);
    projection = projection * glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, -200.0f));

    glViewport(0, 0, width, height);

    glUseProgram(this->shader);
    glUniformMatrix4fv(this->projection_matrix, 1, false, glm::value_ptr(projection));

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    glBindVertexArray(this->vao);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glBindVertexArray(0);
}

void TriangleColoredProgram::Destroy()
{
    glBindVertexArray(this->vao);
    glBindBuffer(GL_ARRAY_BUFFER, this->vbo);
    glDeleteBuffers(1, &this->vbo);
    glBindVertexArray(0);
}

int main()
{
    return TriangleColoredProgram().Run();
}
