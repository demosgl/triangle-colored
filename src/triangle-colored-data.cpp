#include "triangle-colored.h"

glm::vec3 TriangleColoredProgram::vertices[] = {
    glm::vec3(  100.0f, -100.0f, 0.0f), glm::vec3(1.0f, 0.0f, 0.0f),
    glm::vec3(    0.0f,  100.0f, 0.0f), glm::vec3(0.0f, 1.0f, 0.0f),
    glm::vec3( -100.0f, -100.0f, 0.0f), glm::vec3(0.0f, 0.0f, 1.0f)
};
