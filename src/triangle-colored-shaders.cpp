#include "triangle-colored.h"
#include <GL/glextl.h>
#include <iostream>
#include <string>
#include <fstream>
#include <streambuf>

void TriangleColoredProgram::SetupShaders()
{
    auto vertexShader = TriangleColoredProgram::LoadShaderFile("data/default-shader.vert");
    auto fragmentShader = TriangleColoredProgram::LoadShaderFile("data/default-shader.frag");
    const char* v = vertexShader.c_str();
    const char* f = fragmentShader.c_str();

    // Compile and check vertex shader
    GLuint vertShader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertShader, 1, (const GLchar*const*)&v, NULL);
    glCompileShader(vertShader);
    CheckShaderStatus(vertShader);

    // Compile and check fragment shader
    GLuint fragShader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragShader, 1, (const GLchar*const*)&f, NULL);
    glCompileShader(fragShader);
    CheckShaderStatus(fragShader);

    // Link and check program linking
    this->shader = glCreateProgram();
    glAttachShader(this->shader, vertShader);
    glAttachShader(this->shader, fragShader);
    glLinkProgram(this->shader);
    CheckProgramStatus(this->shader);

    // Cleanup after
    glDeleteShader(vertShader);
    glDeleteShader(fragShader);

    glUseProgram(this->shader);
    this->projection_matrix = glGetUniformLocation(this->shader, "projection_matrix");
}

std::string TriangleColoredProgram::LoadShaderFile(const std::string& filename)
{
    std::ifstream t(filename.c_str());
    if (t.is_open())
    {
        return std::string((std::istreambuf_iterator<char>(t)), std::istreambuf_iterator<char>());
    }

    return std::string("");
}

void TriangleColoredProgram::CheckShaderStatus(GLuint shader)
{
    GLint result = GL_FALSE;
    int logLength;

    glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
    if (result == GL_FALSE)
    {
        glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &logLength);
        GLchar* error = new GLchar[(logLength > 1) ? logLength : 1];
        glGetShaderInfoLog(shader, logLength, NULL, &error[0]);
        std::cerr << &error[0] << std::endl;
        delete []error;
    }
    else
        std::cout << "Shader compiled" << std::endl;
}

void TriangleColoredProgram::CheckProgramStatus(GLuint program)
{
    GLint result = GL_FALSE;
    int logLength;

    glGetProgramiv(program, GL_LINK_STATUS, &result);
    if (result == GL_FALSE)
    {
        glGetProgramiv(program, GL_INFO_LOG_LENGTH, &logLength);
        GLchar* error = new GLchar[(logLength > 1) ? logLength : 1];
        glGetProgramInfoLog(program, logLength, NULL, &error[0]);
        std::cerr << &error[0] << std::endl;
        delete []error;
    }
    else
        std::cout << "Program linked" << std::endl;
}
