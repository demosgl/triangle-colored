#include "triangle-colored.h"
#include <GL/glextl.h>

void TriangleColoredProgram::SetupBuffers()
{
    glGenVertexArrays(1, &this->vao);
    glBindVertexArray(this->vao);

    glGenBuffers(1, &this->vbo);
    glBindBuffer(GL_ARRAY_BUFFER, this->vbo);

    glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * 6, 0, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(glm::vec3) * 6, (GLvoid*)TriangleColoredProgram::vertices);

    GLuint vertexAttrib = glGetAttribLocation(shader, "vertex");
    glVertexAttribPointer(vertexAttrib, 3, GL_FLOAT, GL_FALSE, sizeof(glm::vec3) * 2, 0);
    glEnableVertexAttribArray(vertexAttrib);

    GLuint colorAttrib = glGetAttribLocation(shader, "color");
    glVertexAttribPointer(colorAttrib, 4, GL_FLOAT, GL_FALSE, sizeof(glm::vec3) * 2, (GLvoid*)sizeof(glm::vec3));
    glEnableVertexAttribArray(colorAttrib);

    glBindVertexArray(0);
}
